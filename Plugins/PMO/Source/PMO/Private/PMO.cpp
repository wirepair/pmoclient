// Copyright Epic Games, Inc. All Rights Reserved.

#include "PMO.h"
#include "Core.h"
#include "Internationalization/Text.h"
#include "Modules/ModuleManager.h"
#include "Interfaces/IPluginManager.h"
#include "crypto/cryptor.h"

#define LOCTEXT_NAMESPACE "FPMOModule"

void FPMOModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module

	// Get the base directory of this plugin
	FString BaseDir = IPluginManager::Get().FindPlugin("PMO")->GetBaseDir();
	
	FText Dir = FText::FromString(BaseDir);

	// Add on the relative location of the third party dll and load it
	FString LibraryPath;
#if PLATFORM_WINDOWS
	// Plugins\PMO\Source\ThirdParty\pmo\build\src\Release\pmo_library.dll
	LibraryPath = FPaths::Combine(*BaseDir, TEXT("/../../Binaries/Win64/pmo_library.dll"));
	FText LibDir = FText::FromString(LibraryPath);

	// if (FPaths::FileExists(LibraryPath))
	// {
	// 	FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("ThirdPartyLibraryError", "Exists!"));
	// }
	// FMessageDialog::Open(EAppMsgType::Ok, LibDir);
#elif PLATFORM_MAC
    LibraryPath = FPaths::Combine(*BaseDir, TEXT("Source/ThirdParty/PMOLibrary/Mac/Release/libExampleLibrary.dylib"));
#elif PLATFORM_LINUX
	LibraryPath = FPaths::Combine(*BaseDir, TEXT("Binaries/ThirdParty/PMOLibrary/Linux/x86_64-unknown-linux-gnu/libExampleLibrary.so"));
#endif // PLATFORM_WINDOWS

	PMOHandle = !LibraryPath.IsEmpty() ? FPlatformProcess::GetDllHandle(*LibraryPath) : nullptr;

	if (!PMOHandle)
	{
		FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("ThirdPartyLibraryError", "Failed to load PMO library"));
	}
}

void FPMOModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	// Free the dll handle
	FPlatformProcess::FreeDllHandle(PMOHandle);
	PMOHandle = nullptr;
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FPMOModule, PMO)
