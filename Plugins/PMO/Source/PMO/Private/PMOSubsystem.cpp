#include "PMOSubsystem.h"
#include "Core.h"
#include <chrono>
#include "ecs/init.h"
#include "ecs/network/network.h"
#include "Internationalization/Text.h"
#include "Kismet/KismetMathLibrary.h"

flecs::world& UPMOSubsystem::GetEcsWorld() const
{
	return *ECSWorld.Get();
}

void UPMOSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	UE_LOG(LogTemp, Warning, TEXT("UPMOSubsystem Initialize"));

	ECSWorld = MakeUnique<flecs::world>();
	UE_LOG(LogTemp, Warning, TEXT("UPMOSubsystem ECSWorld Initialized"));

	// Generate random user id for now
	UserId = UKismetMathLibrary::RandomIntegerInRange(100000, 9999999);
		
	// https://www.flecs.dev/explorer
	ECSWorld->set<flecs::Rest>({});

	Super::Initialize(Collection);
}

void UPMOSubsystem::InitFlecs(const FString& AssetPath, const FString& MapName, const FString& ServerAddress, const UObject* WorldContextObject)
{
	UE_LOG(LogTemp, Warning, TEXT("Start InitFlecs!"));
	UE_LOG(LogTemp, Warning, TEXT("UPMOSubsystem Config Creation..."));
	// Renderer = nullptr;
	Renderer = MakeUnique<UPMODebugRenderer>(WorldContextObject);
	Asset = std::string(StringCast<ANSICHAR>(*AssetPath).Get());
	Map = std::string(StringCast<ANSICHAR>(*MapName).Get());
	Server = std::string(StringCast<ANSICHAR>(*ServerAddress).Get());

	Config = config::Config("client.log", Asset.c_str(), Map.c_str(), Server.c_str(), 4242, Renderer.Get());

	auto NewPlayerT = InitPMOClient(*ECSWorld.Get(), Logger, Config, UserId);
	if (NewPlayerT == 0)
	{
		UE_LOG(LogTemp, Error, TEXT("UPMOSubsystem Failed to init PMOClient..."));
		return;
	}
	
	NewPlayer = ECSWorld->entity(NewPlayerT);

	OnTickDelegate = FTickerDelegate::CreateUObject(this, &UPMOSubsystem::Tick);
	OnTickHandle = FTSTicker::GetCoreTicker().AddTicker(OnTickDelegate);
	UE_LOG(LogTemp, Warning, TEXT("PMOFlecs System initialized!"));
}

void UPMOSubsystem::Deinitialize()
{
	
	if(!ECSWorld) 
	{
		DeinitPMO(*ECSWorld.Get());
		ECSWorld.Reset();
	}
	
	UE_LOG(LogTemp, Warning, TEXT("UPMOSubsystem reset ECSWorld"));
	if (OnTickHandle.IsValid())
	{
		FTSTicker::GetCoreTicker().RemoveTicker(OnTickHandle);
		UE_LOG(LogTemp, Warning, TEXT("UPMOSubsystem removed Ticker"));
	}

	UE_LOG(LogTemp, Warning, TEXT("UPMOSubsystem has shut down!"));
	Super::Deinitialize();
}

bool UPMOSubsystem::Tick(float DeltaTime)
{
	if (!ECSWorld)
	{
		return true;
	}

	NewPlayer.set<network::Inputs>({UserKeyPresses, CurrentYaw, CurrentPitch, 0 });
	TickPMOClient(*ECSWorld, NewPlayer, Logger, DeltaTime);
	if (NewPlayer.has<network::ClientStateBuffer>())
	{
		UE_LOG(LogTemp, Warning, TEXT("------------------ Tick %d ----------------\n"), NewPlayer.get<network::ClientStateBuffer>()->State.SequenceId);
	}
	auto Start = std::chrono::system_clock::now();
	bool Ret = ECSWorld->progress();
	auto Runtime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - Start);
	auto RuntimeMilli = std::chrono::duration_cast<std::chrono::milliseconds>(Runtime);
	UE_LOG(LogTemp, Warning, TEXT("UPMOSubsystem tick took %dusec/%dms!"), Runtime.count(), RuntimeMilli.count());
	UserKeyPresses.Clear();

	return Ret;
}

void UPMOSubsystem::CollectInput(input::InputType Input, uint16_t Yaw, uint16_t Pitch, bool bPressed)
{
	bPressed ? UserKeyPresses.Press(Input) : UserKeyPresses.Unpress(Input);
	CurrentYaw = Yaw;
	CurrentPitch = Pitch;
}

FVector UPMOSubsystem::GetPlayerLocation() const
{
	if(!NewPlayer.has<units::Vector3>()) 
	{
		return FVector(0.f, 0.f, 0.f);
	}

	auto Position = NewPlayer.get<units::Vector3>();
	return FVector(Position->vec3[0] * 100.f, Position->vec3[2] * 100.f, Position->vec3[1] * 100.f);
}

FVector UPMOSubsystem::GetPlayerVelocity() const
{
	if (!NewPlayer.has<units::Velocity>())
	{
		return FVector(0.f, 0.f, 0.f);
	}

	auto Velocity = NewPlayer.get<units::Velocity>();
	return FVector(Velocity->vec3[0] * 100.f, Velocity->vec3[2] * 100.f, Velocity->vec3[1] * 100.f);
}


FQuat UPMOSubsystem::GetPlayerRotation() const
{
	if(!NewPlayer.has<units::Quat>()) 
	{
		return FQuat::Identity;
	}

	auto Rotation = NewPlayer.get<units::Quat>();
	return FQuat{ Rotation->vec4[0], Rotation->vec4[2], Rotation->vec4[1], Rotation->vec4[3] };
}

const character::PMOCharacter* UPMOSubsystem::GetCharacter() const
{
	return NewPlayer.get<character::PMOCharacter>();
}

uint64_t UPMOSubsystem::GetPlayerEntity() const
{
	return NewPlayer.id();
}