// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System;
using System.IO;
//using OpenTracing;
using UnrealBuildTool.Rules;

public class PMO : ModuleRules
{
	public PMO(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
        CppStandard = CppStandardVersion.Cpp20;

        PublicIncludePaths.AddRange(
			new string[] {
				ModuleDirectory+"/../ThirdParty/pmo/src/",
                ModuleDirectory+"/../ThirdParty/pmo/build/_deps/flatbuffers-src/include/",
				ModuleDirectory+"/../ThirdParty/pmo/build/_deps/fmt-src/include/",
                ModuleDirectory+"/../ThirdParty/pmo/build/_deps/jolt-src/",
                ModuleDirectory+"/../ThirdParty/pmo/third_party/libsodium-1.0.18/src/libsodium/include/",
			});
		
		PrivateIncludePaths.AddRange(
			new string[] {
				
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"PMOLibrary",
				"CoreUObject",
				"Engine",
				"Projects"
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);

	}
}
