#pragma once

#include "Core.h"
#include "Kismet/KismetSystemLibrary.h"

#include "debugrender/debugrender.h"

class PMO_API UPMODebugRenderer : public debug::DebugRenderer
{
public:
    UPMODebugRenderer(const UObject* InWorld) : World(InWorld) {};
\
    virtual void DrawLine(debug::DebugVec3& From, debug::DebugVec3& To, uint32_t InColor) override
    {
        FVector DebugFrom{ From.X, From.Z, From.Y };
        FVector DebugTo{ To.X, To.Z, To.Y };
        auto a = (InColor >> 24) & 0xFF;
        auto b = (InColor >> 16) & 0xFF;
        auto g = (InColor >> 8) & 0xFF;
        auto r = InColor & 0xFF;
        FColor Color(r, g, b, a);

        // Scale by 100 as UE uses cm and Jolt uses meters
        UKismetSystemLibrary::DrawDebugLine(World, DebugFrom * 100.f, DebugTo * 100.f, FLinearColor(Color), 3.f, 1.f);
    };

    virtual void DrawTriangle(debug::DebugVec3& Vec1, debug::DebugVec3& Vec2, debug::DebugVec3& Vec3, uint32_t InColor, bool CastShadow) override
    {
        DrawLine(Vec1, Vec2, InColor);
        DrawLine(Vec2, Vec3, InColor);
        DrawLine(Vec3, Vec1, InColor);
    };

    const UObject *World;
};