#pragma once

#include <cstdint>

#include "Core.h"
#include "Math/Vector.h"
#include "Math/Quat.h"

#include "flecs.h"
#include "ecs/character/pmocharacter.h"
#include "ecs/character/components.h"
#include "config/config.h"
#include "input/input.h"
#include "schemas/client_generated.h"


#include "PMOLogger.h"
#include "PMODebugRenderer.h"

#include "Subsystems/GameInstanceSubsystem.h"
#include "PMOSubsystem.generated.h"


USTRUCT(BlueprintType)
struct FFlecsEntityHandle
{
	GENERATED_USTRUCT_BODY()
	
	FFlecsEntityHandle()  { FlecsEntityId = -1; }
	
	FFlecsEntityHandle(int inId)
	{
		FlecsEntityId = inId;
	}

	UPROPERTY(BlueprintReadWrite)
	int FlecsEntityId;
};

UCLASS()
class PMO_API UPMOSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	
    virtual void Deinitialize() override;
	
	flecs::world& GetEcsWorld() const;

	UFUNCTION(BlueprintCallable, Category="Flecs")
	void InitFlecs(const FString& AssetPath, const FString &MapName, const FString& ServerAddress, const UObject* WorldContextObject);

	uint64_t GetPlayerEntity() const;

	//UFUNCTION(BlueprintCallable, Category="Network")
	void CollectInput(input::InputType Input, uint16_t Yaw, uint16_t Pitch, bool bPressed);

	const character::PMOCharacter* GetCharacter() const;

	UFUNCTION(BlueprintCallable, Category = "Character")
	FVector GetPlayerLocation() const;
	
	UFUNCTION(BlueprintCallable, Category = "Character")
	FQuat GetPlayerRotation() const;

	UFUNCTION(BlueprintCallable, Category = "Character")
	FVector GetPlayerVelocity() const;

protected:
	FTickerDelegate OnTickDelegate;
	FTSTicker::FDelegateHandle OnTickHandle;

	TUniquePtr<flecs::world> ECSWorld{};
	TUniquePtr<UPMODebugRenderer> Renderer = nullptr;
	UWorld* World;
	
	/** Inputs **/
	input::Input UserKeyPresses{};
	uint16_t CurrentYaw{};
	uint16_t CurrentPitch{};
		
	UPMOLogger Logger{};
	flecs::entity NewPlayer{};
	/** Config references, so we don't have pointers go out of scope **/
	std::string Asset;
	std::string Map;
	std::string Server;
	config::Config Config{};
	uint32_t UserId = -1;

private:
	bool Tick(float DeltaTime);
};
