#pragma once

#include "Core.h"
#include "logger/logger.h"

class PMO_API UPMOLogger : public logging::Logger
{
public:
    UPMOLogger() {};

private:
    virtual void LogDebug(const std::string &Msg) override 
    {
        FString LogMsg(Msg.c_str());
        UE_LOG(LogTemp, Verbose, TEXT("%s"), *LogMsg);
    }; 
    
    virtual void LogInfo(const std::string &Msg) override 
    {
        FString LogMsg(Msg.c_str());
        UE_LOG(LogTemp, Log, TEXT("%s"), *LogMsg);
    };
    
    virtual void LogWarn(const std::string &Msg) override 
    {
        FString LogMsg(Msg.c_str());
        UE_LOG(LogTemp, Warning, TEXT("%s"), *LogMsg);
    };

    virtual void LogError(const std::string &Msg) override 
    {
        FString LogMsg(Msg.c_str());
        UE_LOG(LogTemp, Error, TEXT("%s"), *LogMsg);
    };
};