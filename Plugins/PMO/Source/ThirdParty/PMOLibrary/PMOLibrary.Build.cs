// Fill out your copyright notice in the Description page of Project Settings.

using System;
using System.IO;
using UnrealBuildTool;

public class PMOLibrary : ModuleRules
{
	public PMOLibrary(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;


        if (Target.Platform == UnrealTargetPlatform.Win64)
		{
			PublicDefinitions.Add("JPH_DEBUG_RENDERER");
			// Required for flat buffers, otherwise errors++
			PublicDefinitions.Add("NOMINMAX");
			//test
			PublicDefinitions.Add("SODIUM_STATIC=1");
			PublicDefinitions.Add("SODIUM_EXPORT=");

			//PublicDefinitions.Add("flecs_EXPORTS");

			// Add the import library
			PublicAdditionalLibraries.AddRange(new string[] {
				ModuleDirectory+"/../pmo/build/Release/flecs.lib",
				ModuleDirectory+"/../pmo/build/Release/pmo_library.lib",
				//ModuleDirectory+"/../pmo/build/Release/Jolt.lib",
				//ModuleDirectory+"/../pmo/third_party/libsodium-1.0.18/Build/Release/x64/libsodium.lib",
			});

			PublicIncludePaths.AddRange(new string[] {
				ModuleDirectory+"/../pmo/src/",
				ModuleDirectory+"/../pmo/build/_deps/flatbuffers-src/include/",
                ModuleDirectory+"/../pmo/build/_deps/jolt-src/",
                ModuleDirectory+"/../pmo/build/_deps/flecs-src/include/",
				ModuleDirectory+"/../pmo/third_party/libsodium-1.0.18/src/libsodium/include/",
			});

			string binaryPath = ModuleDirectory+"/../../../Binaries/Win64/";
			string destinationDirectory = Path.GetDirectoryName(binaryPath);
			if (!File.Exists(binaryPath))
			{
				Directory.CreateDirectory(destinationDirectory);
            }
			
			File.Copy(ModuleDirectory + "/../pmo/build/Release/flecs.dll", destinationDirectory+"/flecs.dll", true);
            File.Copy(ModuleDirectory + "/../pmo/build/Release/pmo_library.dll", destinationDirectory+"/pmo_library.dll", true);
			File.Copy(ModuleDirectory + "/../pmo/build/Release/Jolt.dll", destinationDirectory + "/Jolt.dll", true);
            File.Copy(ModuleDirectory + "/../pmo/build/Release/spdlog.dll", destinationDirectory + "/spdlog.dll", true);


            // Delay-load the DLL, so we can load it from the right place first
            PublicDelayLoadDLLs.Add("pmo_library.dll");
            //PublicDelayLoadDLLs.Add("flecs.dll");

            // Ensure that the DLL is staged along with the executable
            RuntimeDependencies.Add("$(PluginDir)/Source/ThirdParty/pmo/build/Release/flecs.dll");
            RuntimeDependencies.Add("$(PluginDir)/Source/ThirdParty/pmo/build/Release/Jolt.dll");
            RuntimeDependencies.Add("$(PluginDir)/Source/ThirdParty/pmo/build/Release/spdlog.dll");
            RuntimeDependencies.Add("$(PluginDir)/Source/ThirdParty/pmo/build/Release/pmo_library.dll");

        }
        else if (Target.Platform == UnrealTargetPlatform.Mac)
        {
            // PublicDelayLoadDLLs.Add(Path.Combine(ModuleDirectory, "Mac", "Release", "libExampleLibrary.dylib"));
            // RuntimeDependencies.Add("$(PluginDir)/Source/ThirdParty/PMOLibrary/Mac/Release/libExampleLibrary.dylib");
        }
        else if (Target.Platform == UnrealTargetPlatform.Linux)
		{
			// string ExampleSoPath = Path.Combine("$(PluginDir)", "Binaries", "ThirdParty", "PMOLibrary", "Linux", "x86_64-unknown-linux-gnu", "libExampleLibrary.so");
			// PublicAdditionalLibraries.Add(ExampleSoPath);
			// PublicDelayLoadDLLs.Add(ExampleSoPath);
			// RuntimeDependencies.Add(ExampleSoPath);
		}
	}
}
