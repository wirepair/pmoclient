$binPath = ".\\Plugins\\PMO\\Source\\ThirdParty\\pmo\\build\\lib\\Release\\"

$sourceFiles = @(
    $binPath + "pmo_library.dll"
    $binPath + "flecs.dll"
    $binPath + "fmt.dll"
    $binPath + "Jolt.dll"
)

#$destPath = ".\\Plugins\\PMO\\Binaries\\Win64\\"

$destPath = ".\\Binaries\\Win64\\"
foreach ($file in $sourceFiles) {
    Copy-Item $file -Destination $destPath -Force
}
