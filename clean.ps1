function Remove-Directory {
    param (
        [string]$directoryPath
    )

    $scriptDirectory = $PSScriptRoot
    $fullDirectoryPath = Join-Path -Path $scriptDirectory -ChildPath $directoryPath

    if (Test-Path -Path $fullDirectoryPath -PathType Container) {
        Remove-Item -Path $fullDirectoryPath -Recurse -Force
        Write-Host "Directory '$fullDirectoryPath' and its contents have been deleted."
    } else {
        Write-Host "Directory '$fullDirectoryPath' not found."
    }
}

Write-Host "Removing Binaries."
Remove-Directory -directoryPath "Binaries"
Write-Host "Removing Intermediate."
Remove-Directory -directoryPath "Intermediate"

Write-Host "Removing Plugin Binaries."
Remove-Directory -directoryPath "Plugins\\PMO\\Binaries"
Write-Host "Removing Plugin Intermediate."
Remove-Directory -directoryPath "Plugins\\PMO\\Intermediate"
