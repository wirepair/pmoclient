// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PMOClientCharacter.h"
#include "Actors/NetworkCharacterActor.h"
#include "PMO/Public/PMOSubsystem.h"
#include "GameFramework/GameModeBase.h"
#include "PMOClientGameMode.generated.h"

UCLASS(minimalapi)
class APMOClientGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APMOClientGameMode();

	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;

	virtual void StartPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	flecs::world& GetEcsWorld() { return PMOSystem->GetEcsWorld(); }

	void RegisterPMOSystems();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PMO Config")
	FString ServerAddress;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PMO Config")
	FString AssetPath;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PMO Config")
	FString PMOMapName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ANetworkCharacterActor> CharacterClass;

private:
	bool bInitialized = false;
	TObjectPtr<UPMOSubsystem> PMOSystem;
	TObjectPtr<UWorld> World;
	TObjectPtr<APMOClientCharacter> LocalPlayer;
};



