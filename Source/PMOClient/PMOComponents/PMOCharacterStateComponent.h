// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <flecs.h>
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PMOCharacterStateComponent.generated.h"

// annoying we have to define this in multiple places
UENUM(BlueprintType, DisplayName = "ActionType")
enum class PMOActionType : uint8
{
    NOOP,
    JUMPING,
    BLOCKING,
    PARRYING,

    START_DODGE,
    DODGE_FORWARD,
    DODGE_BACKWARD,
    DODGE_LEFT,
    DODGE_RIGHT,
    END_DODGE,

    START_ATTACK,
    START_SWORDSH_ATTACK,
    SWORDSH_ATTACK_1,
    SWORDSH_ATTACK_2,
    SWORDSH_ATTACK_3,
    SWORDSH_SEC_ATTACK_1,
    SWORDSH_SEC_ATTACK_2,
    SWORDSH_SEC_ATTACK_3,
    END_SWORDSH_ATTACK,

    TWOHAND_ATTACK,
    SPEARSH_ATTACK,
    BOW_ATTACK,
    CAST_ATTACK,
    END_ATTACK,

    START_INCAPACITATED,
    STUNNED,
    SLEEPING,
    KNOCKED_DOWN,
    DEAD,
    END_INCAPACITATED,
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMOCLIENT_API UPMOCharacterStateComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPMOCharacterStateComponent();

	flecs::entity GetFlecsOwner();

	UFUNCTION(BlueprintCallable)
	PMOActionType GetState();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	flecs::entity FlecsOwner;
};
