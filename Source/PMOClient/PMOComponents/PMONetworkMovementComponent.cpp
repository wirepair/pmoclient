// Fill out your copyright notice in the Description page of Project Settings.


#include "PMOClient/PMOComponents/PMONetworkMovementComponent.h"
#include "PMOClient/PMOConverters.h"
#include "PMOClient/Actors/NetworkCharacterActor.h"


// Sets default values for this component's properties
UPMONetworkMovementComponent::UPMONetworkMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UPMONetworkMovementComponent::BeginPlay()
{
	Super::BeginPlay();
	auto World = GetWorld();

	auto EntityComponent = this->GetOwner()->GetComponentByClass<UPMOEntityComponent>();

	if (!EntityComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to get EntityComponent from MovementComponent"));
		return;
	}

	if (!EntityComponent->Entity().is_valid() || !EntityComponent->Entity().is_alive())
	{
		UE_LOG(LogTemp, Error, TEXT("BeginPlay UPMONetworkMovementComponent entity not valid!"));
		return;
	}
	Entity = EntityComponent->Entity();

}


// Called every frame
void UPMONetworkMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

FVector UPMONetworkMovementComponent::GetVelocity() const
{
	if (!Entity.is_valid() || !Entity.is_alive())
	{
		UE_LOG(LogTemp, Warning, TEXT("UPMONetworkMovementComponent GetVelocity Character NOT set"));
		return FVector::ZeroVector;
	}

	auto Character = Entity.get<character::NetworkCharacter>();
	if (!Character)
	{
		return FVector::ZeroVector;
	}
	auto PMOVelocity = Character->GetVelocity();
	auto Vel = PMOConverters::VelocityToFVector(PMOVelocity);
	UE_LOG(LogTemp, Warning, TEXT("UPMONetworkMovementComponent Returning Velocity: %s"), *Vel.ToCompactString());
	return Vel;
}

bool UPMONetworkMovementComponent::IsFalling() const
{
	if (Entity.is_valid() && Entity.has<character::ActionType>())
	{
		// TODO Add ActionType::FALLING
		return *Entity.get<character::ActionType>() == character::ActionType::JUMPING;
	}
	return false;
}

bool UPMONetworkMovementComponent::IsMovingOnGround() const
{
	return !IsFalling();
}
