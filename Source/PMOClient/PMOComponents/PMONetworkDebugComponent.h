// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "PMONetworkDebugComponent.generated.h"

/**
 * 
 */
UCLASS()
class PMOCLIENT_API UPMONetworkDebugComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	UPMONetworkDebugComponent();

	virtual void BeginPlay();

	UPROPERTY(BlueprintReadWrite, Category = "Settings")
	bool bIsEnabled = true;
};
