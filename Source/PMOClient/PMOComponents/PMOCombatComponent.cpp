// Fill out your copyright notice in the Description page of Project Settings.


#include "PMOCombatComponent.h"
#include "PMOCharacterStateComponent.h"
#include "ecs/character/components.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UPMOCombatComponent::UPMOCombatComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UPMOCombatComponent::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("UPMOCombatComponent BeginPlay called!"));
	Character = Cast<ACharacter>(GetOwner());
	StateComponent = GetOwner()->GetComponentByClass<UPMOCharacterStateComponent>();
}

void UPMOCombatComponent::OnClientHit(combat::NotifyDamageEvent& Event)
{
	LastClientHitEvent = FOnHitEvent(Event);
	OnClientHitDelegate.Broadcast(LastClientHitEvent);
	UE_LOG(LogTemp, Warning, TEXT("UPMOCombatComponent::OnClientHitEvent"));
}

void UPMOCombatComponent::OnHit(combat::NetDamage &Event)
{
	LastHitEvent = FOnHitEvent(Event);
	OnHitDelegate.Broadcast(LastHitEvent);
	UE_LOG(LogTemp, Warning, TEXT("UPMOCombatComponent::OnHit Event"));
}

bool UPMOCombatComponent::WasHit(FOnHitEvent &OutDamageEvent, bool bShouldConsume)
{
	return HandleWasHit(LastHitEvent, OutDamageEvent, bShouldConsume);
}

bool UPMOCombatComponent::WasClientHit(FOnHitEvent& OutDamageEvent, bool bShouldConsume)
{
	return HandleWasHit(LastClientHitEvent, OutDamageEvent, bShouldConsume);
}

bool UPMOCombatComponent::HandleWasHit(FOnHitEvent &LastEvent, FOnHitEvent& OutDamageEvent, bool bShouldConsume)
{
	if (LastEvent.DamageType == EDamageClass::INVALID)
	{
		return false;
	}

	OutDamageEvent = FOnHitEvent(LastEvent);

	if (bShouldConsume)
	{
		LastEvent.Clear();
	}
	return true;
}

bool UPMOCombatComponent::IsInCombat() const
{
	if (!StateComponent)
	{
		return false;
	}
	auto State = StateComponent->GetState();
	return State > PMOActionType::START_ATTACK && State < PMOActionType::END_ATTACK;
}

bool UPMOCombatComponent::IsBlocking() const
{
	if (!StateComponent)
	{
		return false;
	}
	return StateComponent->GetState() == PMOActionType::BLOCKING;
}
