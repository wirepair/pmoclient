// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PMO/Public/PMOSubsystem.h"
#include "PMOClient/PMOConverters.h"
#include "Components/ActorComponent.h"
#include "ecs/combat/combat.h"
#include "PMOCombatComponent.generated.h"

UENUM(BlueprintType, DisplayName = "DamageClass")
enum class EDamageClass : uint8
{
    INVALID,
    FALL,
    PEIRCE,
    SLASH,
    BLUNT,
    FIRE,
    ICE,
    WATER,
    LIGHTNING,
    POISON,
    HOLY,
    CORRUPTION,
    BLOOD,
};

/**
* Damage events for both local and network based
*/
USTRUCT(BlueprintType)
struct FOnHitEvent
{
	GENERATED_USTRUCT_BODY()

		FOnHitEvent() { };

	FOnHitEvent(combat::NotifyDamageEvent& Damage)
	{
		// Here's hoping these casts don't break shit!
		Target = static_cast<int64>(Damage.TargetHit);
		DamageType = static_cast<EDamageClass>(Damage.Type);
		HitLocation = PMOConverters::Vector3ToFVector(Damage.HitPosition);
		bIsLocalEvent = true;
	};

	/** For remote events that only have inst/targ/amount/class */
	FOnHitEvent(combat::NetDamage& Damage)
	{
		bIsLocalEvent = false;
		Amount = Damage.Amount;
		// Here's hoping these casts don't break shit!
		Instigator = static_cast<int64>(Damage.Instigator);
		Target = static_cast<int64>(Damage.TargetHit);
		DamageType = static_cast<EDamageClass>(Damage.Type);
	};

	FOnHitEvent(FOnHitEvent& Other)
	{
		Amount = Other.Amount;
		Instigator = Other.Instigator;
		Target = Other.Target;
		DamageType = Other.DamageType;
		bIsLocalEvent = Other.bIsLocalEvent;
		HitLocation = Other.HitLocation;
	};

	void Clear()
	{
		Amount = 0.f;
		DamageType = EDamageClass::INVALID;
		Instigator = 0;
		Target = 0;
		bIsLocalEvent = false;
		HitLocation = FVector::Zero();
	};

	UPROPERTY(BlueprintReadWrite);
	bool bIsLocalEvent{};

	UPROPERTY(BlueprintReadWrite);
	float Amount{};

	UPROPERTY(BlueprintReadWrite);
	EDamageClass DamageType = EDamageClass::INVALID;

	// flecs::entity Blueprint doesn't support uint64???
	UPROPERTY(BlueprintReadWrite);
	int64 Instigator{};

	// flecs::entity
	UPROPERTY(BlueprintReadWrite);
	int64 Target{};

	UPROPERTY(BlueprintReadWrite);
	FVector HitLocation{};
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnHit, FOnHitEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMOCLIENT_API UPMOCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPMOCombatComponent();

	void OnClientHit(combat::NotifyDamageEvent& HitEvent);

	void OnHit(combat::NetDamage& HitEvent);

	/** This should be called to see if we had any local hits (e.g. client executed a hit, 
		but we don't know what the dmg yet is from the server 
	*/
	UFUNCTION(BlueprintCallable, Category = "Hit")
	bool WasClientHit(FOnHitEvent& OutDamageEvent, bool bShouldConsume);

	/** This should be called to see if we had any server confirmed hits */
	UFUNCTION(BlueprintCallable, Category = "Hit")
	bool WasHit(FOnHitEvent& OutDamageEvent, bool bShouldConsume);

	/** For server based hit events */
	FOnHit& GetOnHitDelegate() { return OnHitDelegate; }

	/** For client based hit events */
	FOnHit& GetOnClientHitDelegate() { return OnClientHitDelegate; }

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Actions")
	bool IsInCombat() const;

	UFUNCTION(BlueprintCallable, Category = "Actions")
	bool IsBlocking() const;
	
private:
	bool HandleWasHit(FOnHitEvent& LastEvent, FOnHitEvent& OutDamageEvent, bool bShouldConsume);

private:
	TObjectPtr<ACharacter> Character;
	TObjectPtr<class UPMOCharacterStateComponent> StateComponent;

	FOnHit OnHitDelegate;
	FOnHit OnClientHitDelegate;

	FOnHitEvent LastHitEvent{};
	FOnHitEvent LastClientHitEvent{};

	uint8_t MeleeSwingCount = 0;
};
