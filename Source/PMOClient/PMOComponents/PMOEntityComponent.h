// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "PMO/Public/PMOSubsystem.h"
#include "Components/ActorComponent.h"
#include "PMOEntityComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMOCLIENT_API UPMOEntityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPMOEntityComponent();

	virtual void InitializeComponent();

	/** Sets our Flecs entity Ids on spawn, should be the same as server */
	void SetId(const uint64_t FlecsEntityId)
	{
		EntityId = FlecsEntityId;
		UE_LOG(LogTemp, Error, TEXT("UPMOEntityComponent SetIds initalized!"));
	}

	flecs::entity Entity() const
	{
		if (!PMOSystem)
		{
			return flecs::entity{};
		}
		return PMOSystem->GetEcsWorld().entity(this->EntityId);
	}

	UFUNCTION(BlueprintCallable, Category = "PMO Details")
	int64 GetEntityId() { return static_cast<int64>(EntityId); };

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	TObjectPtr<UPMOSubsystem> PMOSystem;
	uint64_t EntityId{};
};
