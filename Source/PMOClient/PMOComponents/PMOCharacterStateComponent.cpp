// Fill out your copyright notice in the Description page of Project Settings.


#include "PMOCharacterStateComponent.h"
#include "PMOEntityComponent.h"
#include "PMOClient/Actors/NetworkCharacterActor.h"

// Sets default values for this component's properties
UPMOCharacterStateComponent::UPMOCharacterStateComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

flecs::entity UPMOCharacterStateComponent::GetFlecsOwner()
{
	if (FlecsOwner.is_valid() && FlecsOwner.id() != 0)
	{
		return FlecsOwner;
	}

	auto EntityComponent = GetOwner()->GetComponentByClass<UPMOEntityComponent>();
	if (!EntityComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("UPMOCharacterStateComponent Failed to get Owner entity!"));
		return flecs::entity{};
	}

	FlecsOwner = EntityComponent->Entity();

	return FlecsOwner;
}


// Called when the game starts
void UPMOCharacterStateComponent::BeginPlay()
{
	Super::BeginPlay();
}

PMOActionType UPMOCharacterStateComponent::GetState()
{
	auto Owner = GetFlecsOwner();
	//auto IsNetwork = Cast<ANetworkCharacterActor>(GetOwner());
	if (Owner == 0 || !Owner.has<character::ActionType>())
	{
		/*if (IsNetwork)
		{
			UE_LOG(LogTemp, Error, TEXT("UPMOCharacterStateComponent returning NOOP No ActionType!!"));
		}*/
		return PMOActionType::NOOP;
	}

	auto PMOAction = Owner.get<character::ActionType>();
	/*if (IsNetwork)
	{
		UE_LOG(LogTemp, Warning, TEXT("UPMOCharacterStateComponent returning %d!"), static_cast<int>(*PMOAction));
	}*/
	return static_cast<PMOActionType>(*PMOAction);
}
