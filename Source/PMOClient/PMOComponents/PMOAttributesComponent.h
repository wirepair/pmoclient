// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ecs/units.h"
#include "Components/ActorComponent.h"
#include "PMOAttributesComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnAttributesUpdated);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMOCLIENT_API UPMOAttributesComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPMOAttributesComponent();

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetHealth();

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetShield();

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetMana();

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float GetStamina();

	void Update(units::Attributes& UpdatedAttributes, units::TraitAttributes& UpdatedTraits);

	/** TODO: We probably want to use flecs tags here of IsDieing/IsDead instead */
	UFUNCTION(BlueprintCallable, Category = "States")
	bool IsDead();

	flecs::entity GetFlecsOwner();

	units::Attributes& GetAttributes() { return Attributes; }

	units::TraitAttributes& GetTraits() { return Traits; }

	FOnAttributesUpdated& GetAttributesUpdatedDelegate() { return AttributesUpdatedDelegate; };

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	units::Attributes Attributes;
	units::TraitAttributes Traits;
	flecs::entity FlecsOwner;

	FOnAttributesUpdated AttributesUpdatedDelegate;
};
