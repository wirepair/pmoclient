// Fill out your copyright notice in the Description page of Project Settings.


#include "PMOAttributesComponent.h"
#include "PMOEntityComponent.h"

// Sets default values for this component's properties
UPMOAttributesComponent::UPMOAttributesComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

flecs::entity UPMOAttributesComponent::GetFlecsOwner()
{
	if (FlecsOwner.is_valid() && FlecsOwner.id() != 0)
	{
		return FlecsOwner;
	}

	auto EntityComponent = GetOwner()->GetComponentByClass<UPMOEntityComponent>();
	if (EntityComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT("UPMOAttributesComponent got CombatOwner and %d!, entity: %d"), FlecsOwner.is_valid(), EntityComponent->Entity().id());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UPMOAttributesComponent Failed to get Owner entity!"));
	}
	FlecsOwner = EntityComponent->Entity();

	return FlecsOwner;
}

// Called when the game starts
void UPMOAttributesComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

bool UPMOAttributesComponent::IsDead()
{
	return GetHealth() <= 0.f;
}

void UPMOAttributesComponent::Update(units::Attributes& UpdatedAttributes, units::TraitAttributes& UpdatedTraits)
{
	// check before we overwrite
	bool bHasUpdated = (Attributes != UpdatedAttributes || Traits != UpdatedTraits);

	Attributes = UpdatedAttributes;
	Traits = UpdatedTraits;

	if (bHasUpdated)
	{
		AttributesUpdatedDelegate.Broadcast();
	}

	if (!GetFlecsOwner().is_valid() || !FlecsOwner.has<character::ActionType>())
	{
		return;
	}
	// get our state to check if we are already dead
	auto State = FlecsOwner.get<character::ActionType>();
	//UE_LOG(LogTemp, Error, TEXT("UPMOAttributesComponent Update, owner is valid and has actiontype %d!"), static_cast<uint8_t>(*State));
	if (IsDead() && *State != character::ActionType::DEAD)
	{
		FlecsOwner.add(character::ActionType::DEAD);
	}
}

float UPMOAttributesComponent::GetHealth()
{
	return Attributes.Health;
}

float UPMOAttributesComponent::GetShield()
{
	return Attributes.Shield;
}

float UPMOAttributesComponent::GetMana()
{
	return Attributes.Mana;
}

float UPMOAttributesComponent::GetStamina()
{
	return Attributes.Stamina;
}

// Called every frame
void UPMOAttributesComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

