// Fill out your copyright notice in the Description page of Project Settings.


#include "PMOMovementComponent.h"
#include "Math/UnrealMathUtility.h"
#include "PMOClient/PMOClientCharacter.h"
#include "PMOClient/PMOConverters.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UPMOMovementComponent::UPMOMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UPMOMovementComponent::InitializeComponent()
{
	UE_LOG(LogTemp, Error, TEXT("UPMOEntityComponent InitializeComponent called!"));
	auto GameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	if (GameInstance)
	{
		PMOSystem = GameInstance->GetSubsystem<UPMOSubsystem>();
		if (!PMOSystem)
		{
			UE_LOG(LogTemp, Error, TEXT("UPMOMovementComponent Failed to get UPMOSubsystem!"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UPMOMovementComponent Failed to get GameInstance!"));
	}
}	
// Called when the game starts
void UPMOMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void UPMOMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	auto Player = Cast<APMOClientCharacter>(this->GetOwner());

	if (!Player)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to get Player from MovementComponent"));
		return;
	}
	/*FVector NewLocation = FMath::VInterpTo(LastLocation, CurrentLocation, DeltaTime, 1.f);
	FRotator NewRotation = FMath::RInterpTo(LastRotation.Rotator(), CurrentRotation.Rotator(), DeltaTime, 1.f);
	*/

	if (CurrentLocation == LastLocation && CurrentRotation == LastRotation)
	{
		return;
	}

	Player->SetActorLocationAndRotation(CurrentLocation, CurrentRotation);
	LastLocation = CurrentLocation;
	LastRotation = CurrentRotation;
}

void UPMOMovementComponent::UpdateLocationAndRotation(FVector& Location, FVector& Vel, FQuat& Rotation)
{
	auto Player = Cast<APMOClientCharacter>(this->GetOwner());

	if (!Player)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to get Player from MovementComponent"));
		return;
	}

	PlayerVelocity = Vel;
	UpdateComponentVelocity();


	//UE_LOG(LogTemp, Warning, TEXT("Got Player Location: %s Rotation: %s, Velocity: %s"), *Location.ToString(), *Rotation.ToString(), *Vel.ToString());
	Location.Z += Player->CapsuleSize;
	//Player->SetActorLocationAndRotation(Location, Rotation);
	CurrentLocation = Location;
	CurrentRotation = Rotation;

}

void UPMOMovementComponent::ProcessMoveInputs(const FVector2D MovementVector, const uint16_t Yaw, const uint16_t Pitch)
{
	LastYaw = Yaw;
	LastPitch = Pitch;

	// Handle Forward/Backward
	if (MovementVector.Y > 0)
	{
		bool bPressed = true;
		CollectInput(input::InputType::Forward, bPressed);
		//UE_LOG(LogTemp, Warning, TEXT("Moving Forward"));
	}
	else if (FMath::IsNearlyZero(MovementVector.Y))
	{
		bool bPressed = false;
		CollectInput(input::InputType::YAxisMovement, bPressed);
		//UE_LOG(LogTemp, Warning, TEXT("No longer moving Forward/Backward"));
	}
	else if (MovementVector.Y < 0)
	{
		bool bPressed = true;
		CollectInput(input::InputType::Backward, bPressed);
		//UE_LOG(LogTemp, Warning, TEXT("Moving Backward"));
	}

	// Handle Left/Right
	if (MovementVector.X > 0)
	{
		bool bPressed = true;
		CollectInput(input::InputType::Right, bPressed);
		//UE_LOG(LogTemp, Warning, TEXT("Moving Right"));
	}
	else if (FMath::IsNearlyZero(MovementVector.X))
	{
		bool bPressed = false;
		CollectInput(input::InputType::XAxisMovement, bPressed);
		//UE_LOG(LogTemp, Warning, TEXT("No longer moving Left/Right"));
	}
	else if (MovementVector.X < 0)
	{
		bool bPressed = true;
		CollectInput(input::InputType::Left, bPressed);
		//UE_LOG(LogTemp, Warning, TEXT("Moving Left"));
	}
}

void UPMOMovementComponent::CollectInput(const input::InputType Input, const bool bPressed)
{
	if (!PMOSystem)
	{
		UE_LOG(LogTemp, Error, TEXT("CollectInput PMOSystem Null"));
		return;
	}

	PMOSystem->CollectInput(Input, LastYaw, LastPitch, bPressed);
}

FVector UPMOMovementComponent::GetVelocity() const
{
	return PlayerVelocity;
}

bool UPMOMovementComponent::IsFalling() const
{
	auto State = character::GroundState::InAir == GetGroundState();
	return State;
}

bool UPMOMovementComponent::IsMovingOnGround() const
{
	auto State = character::GroundState::OnGround == GetGroundState();
	return State;
}

character::GroundState UPMOMovementComponent::GetGroundState() const
{
	if (!PMOSystem)
	{
		return character::GroundState::InAir;
	}

	auto Char = PMOSystem->GetCharacter();
	if (!Char)
	{
		return character::GroundState::InAir;
	}
	return Char->GetGroundState();
}