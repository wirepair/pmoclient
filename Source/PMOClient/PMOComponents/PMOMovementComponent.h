// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PMO/Public/PMOSubsystem.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/ActorComponent.h"
#include "PMOMovementComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMOCLIENT_API UPMOMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPMOMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	void UpdateLocationAndRotation(FVector& Position, FVector& Velocity, FQuat& Rotation);

	/** Reads in our MovementVector and Yaw/Pitch to send inputs to the PMOSubsystem */
	void ProcessMoveInputs(const FVector2D MovementVector, const uint16_t Yaw, const uint16_t Pitch);

	/** Uses last seen yaw/pitch */
	void CollectInput(const input::InputType Input, const bool bPressed);

	/** Calls into Jolt (via ECS) to get our velocity */
	FVector GetVelocity() const;

	virtual void InitializeComponent() override;

	/** Returns Jolt Physics characters state **/
	virtual bool IsFalling() const override;

	/** We don't want UE5 physics to do anything here so just do nothing */
	virtual void StartNewPhysics(float deltaTime, int32 Iterations) override {};
	
	/** Returns Jolt Physics characters state **/
	virtual bool IsMovingOnGround() const override;

	character::GroundState GetGroundState() const;

protected:

	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame moves the character
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	TObjectPtr<UPMOSubsystem> PMOSystem;

	uint16_t LastYaw{};
	uint16_t LastPitch{};
	
	FVector PlayerVelocity{};
	
	FVector CurrentLocation{};
	FVector LastLocation{};

	FQuat CurrentRotation{};
	FQuat LastRotation{};
};
