// Fill out your copyright notice in the Description page of Project Settings.


#include "PMOClient/PMOComponents/PMOEntityComponent.h"


// Sets default values for this component's properties
UPMOEntityComponent::UPMOEntityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	this->bWantsInitializeComponent = true;
}

void UPMOEntityComponent::InitializeComponent()
{
	UE_LOG(LogTemp, Error, TEXT("UPMOEntityComponent InitializeComponent called!"));
	auto World = GetWorld();
	if (!World)
	{
		UE_LOG(LogTemp, Error, TEXT("UPMOEntityComponent Failed to get World!"));
		return;
	}
	auto GameInstance = UGameplayStatics::GetGameInstance(World);
	if (!GameInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("UPMOEntityComponent Failed to get GameInstance!"));
		return;
	}

	PMOSystem = GameInstance->GetSubsystem<UPMOSubsystem>();

	if (!PMOSystem)
	{
		UE_LOG(LogTemp, Error, TEXT("UPMOEntityComponent Failed to get PMOSubSystem!"));
		return;
	}
}

// Called when the game starts
void UPMOEntityComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UPMOEntityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

