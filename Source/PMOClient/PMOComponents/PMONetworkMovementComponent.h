// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ecs/character/network.h"
#include "PMO/Public/PMOSubsystem.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/ActorComponent.h"
#include "PMONetworkMovementComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PMOCLIENT_API UPMONetworkMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPMONetworkMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	/** Calls into Jolt (via ECS) to get our velocity */
	FVector GetVelocity() const;

	bool IsDead() const;

	/** Returns Jolt Physics characters state **/
	virtual bool IsFalling() const override;

	/** Returns Jolt Physics characters state **/
	virtual bool IsMovingOnGround() const override;

	character::GroundState GetGroundState() const;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	flecs::entity Entity;
	FVector LastLocation;
	FQuat LastRotation;
};
