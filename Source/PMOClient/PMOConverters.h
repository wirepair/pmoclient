#pragma once

#include "CoreMinimal.h"
#include "ecs/units.h"

struct PMOConverters
{
	static inline FVector Vector3ToFVector(units::Vector3& Pos)
	{
		return FVector{ Pos.vec3[0] * 100.f, Pos.vec3[2] * 100.f, Pos.vec3[1] * 100.f };
	}

	static inline FVector VelocityToFVector(units::Velocity& Vel)
	{
		return FVector{ Vel.vec3[0] * 100.f, Vel.vec3[2] * 100.f, Vel.vec3[1] * 100.f };
	}

	static inline FQuat QuatToFQuat(units::Quat& Rot)
	{
		return FQuat{ Rot.vec4[0], Rot.vec4[2], Rot.vec4[1], Rot.vec4[3] };
	}
};