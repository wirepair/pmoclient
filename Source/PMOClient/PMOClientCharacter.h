// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PMO/Public/PMOSubsystem.h"
#include "PMOComponents/PMOEntityComponent.h"
#include "PMOComponents/PMOAttributesComponent.h"
#include "PMOComponents/PMOMovementComponent.h"
#include "PMOComponents/PMOCombatComponent.h"
#include "PMOComponents/PMONetworkDebugComponent.h"
#include "PMOComponents/PMOCharacterStateComponent.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "PMOClientCharacter.generated.h"


UCLASS(config=Game)
class APMOClientCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PMO, meta = (AllowPrivateAccess = "true"))
	class UPMOEntityComponent* EntityComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PMO, meta = (AllowPrivateAccess = "true"))
	class UPMOAttributesComponent* AttributesComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PMO, meta = (AllowPrivateAccess = "true"))
	class UPMOCombatComponent* CombatComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PMO, meta = (AllowPrivateAccess = "true"))
	class UPMOCharacterStateComponent* CharacterStateComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = PMO, meta = (AllowPrivateAccess = "true"))
	class UPMONetworkDebugComponent* NetworkDebugComponent;

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* MoveAction;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* LookAction;

	/** Mouse Actions */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* LeftMouseAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* MiddleMouseAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* RightMouseAction;

	/** Input Actions (Abilities) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* DodgeAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionOneAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionTwoAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionThreeAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionFourAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionFiveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionSixAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionSevenAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionEightAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionNineAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* ActionTenAction;


public:
	APMOClientCharacter(const class FObjectInitializer& ObjectInitializer);

	virtual void Jump() override;

	virtual void StopJumping() override;

	virtual FVector GetVelocity() const override;

	/** Handles all of our inputs */
	void InputAction(input::InputType Input);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PMO)
	float CapsuleSize = 90.f;

protected:
	template<typename T, typename V>
	void CompressFloat(T Value, T Min, T Max, T Resolution, V &Result)
	{
		const T Delta = Max - Min;
		const T Values = Delta / Resolution;
		const uint32_t maxIntegerValue = (uint32_t) ceil( Values );

		V IntegerValue = 0; 
		T NormalizedValue = FMath::Clamp(( Value - Min ) / Delta, 0.0f, 1.0f);
		Result = (V)floor(NormalizedValue * maxIntegerValue + 0.5f);
	}

	template<typename T, typename V>
	void UncompressFloat(V Value, T Min, T Max, T Resolution, V& Result)
	{
		const T Delta = Max - Min;
		const T values = Delta / Resolution;
		const uint32_t MaxIntegerValue = (uint32_t) ceil( values );
		Result = Value / T(MaxIntegerValue) * Delta + Min;
	}


	/** Called for movement input */

	void Move(const FInputActionValue& Value);

	void StopMoving(const FInputActionValue& Value);

	/** Called for looking input, also calls ProcessXAxisMovement if moving and turning */
	void Look(const FInputActionValue& Value);

	bool bMoving = false;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// To add mapping context
	virtual void BeginPlay();

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

private:
	TObjectPtr<UPMOSubsystem> PMOSystem;
};

