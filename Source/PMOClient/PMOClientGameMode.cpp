// Copyright Epic Games, Inc. All Rights Reserved.

#include "PMOClientGameMode.h"
#include "PMO.h"
#include "PMOConverters.h"
#include "PMOClient/Actors/NetworkCharacterActor.h"
#include "PMOComponents/PMONetworkDebugComponent.h"
#include "PMOComponents/PMOAttributesComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"

#include "flecs.h"
#include "ecs/character/network.h"
#include "ecs/network/network.h"
#include "ecs/units.h"
#include "UObject/ConstructorHelpers.h"

APMOClientGameMode::APMOClientGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Characters/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void APMOClientGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
	World = GetWorld();
	if (!World)
	{
		UE_LOG(LogTemp, Error, TEXT("APMOClientGameMode Failed to get GEngine->GetWorld from StartPlay!"));

		return;
	}
	auto GameInstance = UGameplayStatics::GetGameInstance(World);
	if (!GameInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("APMOClientGameMode Failed to get GameInstance!"));
		return;
	}

	if (ServerAddress.IsEmpty())
	{
		UE_LOG(LogTemp, Error, TEXT("Please set ServerAddress!"));
		return;
	}

	if (PMOMapName.IsEmpty())
	{
		UE_LOG(LogTemp, Error, TEXT("Please set PMOMapName!"));
		return;
	}

	if (AssetPath.IsEmpty())
	{
		UE_LOG(LogTemp, Error, TEXT("Please set AssetPath!"));
		return;
	}

	PMOSystem = GameInstance->GetSubsystem<UPMOSubsystem>();

	if (!PMOSystem)
	{
		UE_LOG(LogTemp, Error, TEXT("APMOClientGameMode Failed to get PMOSubSystem!"));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("Calling InitFlecs to start world"));
	PMOSystem->InitFlecs(AssetPath, PMOMapName, ServerAddress, this);

	bInitialized = true;
}


void APMOClientGameMode::StartPlay()
{
	if (!bInitialized)
	{
		UE_LOG(LogTemp, Warning, TEXT("GameMode failed to initalize, exiting"));
		GEngine->Exec(GetWorld(), TEXT("quit"));
		return;
	}

	Super::StartPlay();
	LocalPlayer = Cast<APMOClientCharacter>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetCharacter());
	if (!LocalPlayer)
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to get Local Player from controller"));
	}

	UE_LOG(LogTemp, Warning, TEXT("Calling RegisterPMOSystems"));
	RegisterPMOSystems();
}

void APMOClientGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	UE_LOG(LogTemp, Warning, TEXT("EndPlay ShutdownModule called!"));
}

void APMOClientGameMode::RegisterPMOSystems()
{
	flecs::world& FlecsWorld = PMOSystem->GetEcsWorld();
	UE_LOG(LogTemp, Warning, TEXT("GameMode registering OnNewNetworkCharacter system!"));

	FlecsWorld.system<character::PMOCharacter>("OnUpdatedMovement")
		.kind(flecs::PostUpdate)
		.each([&](flecs::iter & It, size_t Index, character::PMOCharacter & Character)
			{
				if (!LocalPlayer)
				{
					return;
				}

				auto Component = LocalPlayer->GetComponentByClass<UPMOMovementComponent>();
				if (!Component)
				{
					UE_LOG(LogTemp, Warning, TEXT("UpdateLocalAttributes error player does not have UPMOAttributesComponent!"));
					return;
				}

				auto Position = Character.GetPosition();
				auto FPos = PMOConverters::Vector3ToFVector(Position);

				auto Rotation = Character.GetRotation();
				auto FRot = PMOConverters::QuatToFQuat(Rotation);

				auto Velocity = Character.GetVelocity();
				auto FVel = PMOConverters::VelocityToFVector(Velocity);

				Component->UpdateLocationAndRotation(FPos, FVel, FRot);
			});

	FlecsWorld.system<character::NetworkCharacter>("OnNewNetworkCharacter")
		.write<NetworkCharacterRef>()
		.without<NetworkCharacterRef>() // filters only new network characters
		.immediate()
		.each([&](flecs::iter& It, size_t Index, character::NetworkCharacter& NetCharacter)
			{
				auto Position = NetCharacter.GetPosition();
				auto FPos = PMOConverters::Vector3ToFVector(Position);
				auto Rotation = NetCharacter.GetRotation();
				auto FRot = PMOConverters::QuatToFQuat(Rotation);

				const FTransform Transform(FRot, FPos, FVector{ 1.f,1.f,1.f });

				auto AActor = World->SpawnActorDeferred<ANetworkCharacterActor>(CharacterClass.Get(), Transform);
				auto NetworkCharacterPlayer = It.entity(Index);
				if (!AActor)
				{
					UE_LOG(LogTemp, Error, TEXT("OnNewNetworkCharacter failed to SpawnActorDeferred!"));
					return;
				}
				AActor->EntityComponent->SetId(NetworkCharacterPlayer.id());
				NetworkCharacterPlayer.set<NetworkCharacterRef>({AActor});
				AActor->FinishSpawning(Transform);
				UE_LOG(LogTemp, Warning, TEXT("OnNewNetworkCharacter Pos: %s Rot: %s ServerId: %d is_valid: %d"), *FPos.ToCompactString(), *FRot.ToRotationVector().ToCompactString(), NetworkCharacterPlayer.id());
			});

	FlecsWorld.system<NetworkCharacterRef, units::Vector3, units::Velocity, units::Quat, units::Attributes, units::TraitAttributes>("UpdateNetCharacters")
		.kind(flecs::PreUpdate)
		.each([&](flecs::iter& It, size_t Index, NetworkCharacterRef& NetActor, units::Vector3& Pos, units::Velocity& Vel, units::Quat& Rot, units::Attributes& Attributes, units::TraitAttributes& Traits)
		{
			
			FVector FPos = PMOConverters::Vector3ToFVector(Pos);
			FVector FVel = PMOConverters::VelocityToFVector(Vel);
			FQuat FRot = PMOConverters::QuatToFQuat(Rot);

			auto NetworkCharacterPlayer = It.entity(Index);
			auto ServerId = NetworkCharacterPlayer.id();
			UE_LOG(LogTemp, Warning, TEXT("UpdateNetCharacters called NewPos: %s NewVel: %s NewRot: %s %d"), *FPos.ToCompactString(), *FVel.ToCompactString(), *FRot.ToString(), ServerId);
			auto RefActor = static_cast<ANetworkCharacterActor*>(NetActor.Actor);
			RefActor->Update(FPos, FVel, FRot);
			RefActor->UpdateStats(Attributes, Traits);
		});

	FlecsWorld.system<character::PMOCharacter, network::ServerPosition, units::Attributes, units::TraitAttributes>("UpdateLocalAttributes")
		.kind(flecs::PreUpdate)
		.each([&](flecs::iter & It, size_t Index, character::PMOCharacter &Char, network::ServerPosition &NetPos, units::Attributes &Attributes, units::TraitAttributes &Traits)
		{
			if (!LocalPlayer)
			{
				UE_LOG(LogTemp, Warning, TEXT("UpdateLocalAttributes error LocalPlayer not set!"));
				return;
			}
			
			auto Component = LocalPlayer->GetComponentByClass<UPMOAttributesComponent>();
			if (!Component)
			{
				UE_LOG(LogTemp, Warning, TEXT("UpdateLocalAttributes error player does not have UPMOAttributesComponent!"));
				return;
			}
			Component->Update(Attributes, Traits);
			auto Debug = LocalPlayer->GetComponentByClass<UPMONetworkDebugComponent>();
			if (Debug && Debug->bIsEnabled)
			{
				FVector ServerLoc = PMOConverters::Vector3ToFVector(NetPos.Position);
				Debug->SetWorldLocation(ServerLoc);
				//UE_LOG(LogTemp, Error, TEXT("UpdateLocalAttributes ServerPos: %s LocalPos: %s!"), *ServerLoc.ToString(), *LocalPlayer->GetActorLocation().ToString());
			}
		});

	FlecsWorld.system<combat::NotifyDamageEvent>("OnClientDamageEvent")
		.kind(flecs::PostUpdate)
		.write<NetworkCharacterRef>()
		.each([&](flecs::iter& It, size_t Index, combat::NotifyDamageEvent& Event)
		{
			if (Event.TargetHit.has<NetworkCharacterRef>())
			{
				auto Ref = Event.TargetHit.get<NetworkCharacterRef>();
				if (Ref)
				{
					Ref->Actor->GetComponentByClass<UPMOCombatComponent>()->OnClientHit(Event);
				}
				else
				{
					UE_LOG(LogTemp, Error, TEXT("TargetHit Failed to get NetworkActor Ref from OnDamage Event"));
				}
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("Failed to find TargetHit %d"), Event.TargetHit.id());
			}

			UE_LOG(LogTemp, Warning, TEXT("OnDamage combat event: TargetHit: %d"), Event.TargetHit.id());
			// Remove it after handling it
			It.entity(Index).destruct();
		});

	FlecsWorld.system<combat::NetDamage>("OnDamage")
		.kind(flecs::PostUpdate)
		.write<NetworkCharacterRef>()
		.each([&](flecs::iter& It, size_t Index, combat::NetDamage& Event)
		{
			if (!LocalPlayer)
			{
				UE_LOG(LogTemp, Warning, TEXT("OnDamage error LocalPlayer not set!"));
				return;
			}

			auto FlecsId = FlecsWorld.entity(Event.TargetHit.id());
			auto LocalPlayerId = LocalPlayer->GetComponentByClass<UPMOEntityComponent>()->Entity().id();
			if (LocalPlayerId == Event.TargetHit.id())
			{
				LocalPlayer->GetComponentByClass<UPMOCombatComponent>()->OnHit(Event);
				UE_LOG(LogTemp, Warning, TEXT("Dispatched to LocalPlayer TargetHit %d"), Event.TargetHit.id());
			}
			else if (Event.TargetHit.has<NetworkCharacterRef>())
			{
				auto Ref = Event.TargetHit.get<NetworkCharacterRef>(); 
				if (Ref)
				{
					Ref->Actor->GetComponentByClass<UPMOCombatComponent>()->OnHit(Event);
				}
				else
				{
					UE_LOG(LogTemp, Error, TEXT("TargetHit Failed to get NetworkActor Ref from OnDamage Event"));
				}
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("Failed to find TargetHit %d (not local or remote player) %d %d LocalPlayerId: %d"), Event.TargetHit.id(), Event.TargetHit.has<character::NetworkCharacter>(), Event.TargetHit.has<NetworkCharacterRef>(), LocalPlayerId);
			}

			UE_LOG(LogTemp, Warning, TEXT("OnDamage combat event: TargetHit: %d, Instigator: %d, Amount: %f"), Event.TargetHit.id(), Event.Instigator.id(), Event.Amount);
			// Remove it after handling it
			It.entity(Index).destruct();
		});
}