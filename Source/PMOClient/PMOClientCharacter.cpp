// Copyright Epic Games, Inc. All Rights Reserved.

#include "PMOClientCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"


//////////////////////////////////////////////////////////////////////////
// APMOClientCharacter

APMOClientCharacter::APMOClientCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer.SetDefaultSubobjectClass<UPMOMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// We probably don't want to remove the capsule as something may depend on it, but lets set it to zero
	// so it doesn't impact us
	GetCapsuleComponent()->InitCapsuleSize(0.f, 0.f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	//GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	//GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	//// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	//// instead of recompiling to adjust them
	//GetCharacterMovement()->JumpZVelocity = 700.f;
	//GetCharacterMovement()->AirControl = 0.35f;
	//GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	//GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	//Movement = CreateDefaultSubobject<UPMOMovementComponent>(TEXT("PMOMovement"));

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	EntityComponent = CreateDefaultSubobject<UPMOEntityComponent>(TEXT("EntityComponent"));
	AttributesComponent = CreateDefaultSubobject<UPMOAttributesComponent>(TEXT("AttributesComponent"));
	CharacterStateComponent = CreateDefaultSubobject<UPMOCharacterStateComponent>(TEXT("CharacterStateComponent"));
	CombatComponent = CreateDefaultSubobject<UPMOCombatComponent>(TEXT("CombatComponent"));
	NetworkDebugComponent = CreateDefaultSubobject<UPMONetworkDebugComponent>(TEXT("NetworkDebugComponent"));

	FDetachmentTransformRules DetachmentRules(EDetachmentRule::KeepWorld, true);
	NetworkDebugComponent->DetachFromComponent(DetachmentRules);
}

void APMOClientCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	auto World = GetWorld();
	if (World)
	{
		auto GameInstance = UGameplayStatics::GetGameInstance(World);
		if (GameInstance)
		{
			PMOSystem = GameInstance->GetSubsystem<UPMOSubsystem>();
			EntityComponent->SetId(PMOSystem->GetPlayerEntity());
			UE_LOG(LogTemp, Error, TEXT("APMOClientCharacter Set EntityId %d"), PMOSystem->GetPlayerEntity());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("APMOClientCharacter Failed to get GameInstance!"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to get GEngine->GetWorld from BeginPlay!"));
	}

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void APMOClientCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		//Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &APMOClientCharacter::StopJumping);

		//Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::Move);
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Completed, this, &APMOClientCharacter::StopMoving);

		//Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::Look);

		// Mouse Actions
		EnhancedInputComponent->BindAction(LeftMouseAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::MouseLeft);
		EnhancedInputComponent->BindAction(MiddleMouseAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::MouseMiddle);
		EnhancedInputComponent->BindAction(RightMouseAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::MouseRight);
		
		// Ability Actions
		EnhancedInputComponent->BindAction(DodgeAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::Dodge);

		EnhancedInputComponent->BindAction(ActionOneAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionOne);
		EnhancedInputComponent->BindAction(ActionTwoAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionTwo);
		EnhancedInputComponent->BindAction(ActionThreeAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionThree);
		EnhancedInputComponent->BindAction(ActionFourAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionFour);
		EnhancedInputComponent->BindAction(ActionFiveAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionFive);
		EnhancedInputComponent->BindAction(ActionSixAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionSix);
		EnhancedInputComponent->BindAction(ActionSevenAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionSeven);
		EnhancedInputComponent->BindAction(ActionEightAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionEight);
		EnhancedInputComponent->BindAction(ActionNineAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionNine);
		EnhancedInputComponent->BindAction(ActionTenAction, ETriggerEvent::Triggered, this, &APMOClientCharacter::InputAction, input::ActionTen);
	}
	
}

void APMOClientCharacter::Jump()
{
	bPressedJump = true;
	JumpKeyHoldTime = 0.0f;
	auto Movement = Cast<UPMOMovementComponent>(GetCharacterMovement());
	if (!Movement)
	{
		return;
	}
	Movement->CollectInput(input::InputType::Jump, bPressedJump);
}

void APMOClientCharacter::StopJumping()
{
	bPressedJump = false;
}

FVector APMOClientCharacter::GetVelocity() const
{
	auto Movement = Cast<UPMOMovementComponent>(GetCharacterMovement());
	if (!Movement)
	{
		return FVector::Zero();
	}

	return Movement->GetVelocity();
}

void APMOClientCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();
	if (Controller == nullptr)
	{
		return;
	}

	bMoving = true;
	// find out which way is forward
	const FRotator Rotation = Controller->GetControlRotation();

	uint16_t CompressedYaw = static_cast<uint16_t>(Rotation.Yaw);
	const double Yaw = static_cast<double>(CompressedYaw);
	const FRotator YawRotation(0, Yaw, 0);

	uint16_t CompressedPitch = static_cast<uint16_t>(Rotation.Pitch);
	const double Pitch = static_cast<double>(CompressedPitch);

	UE_LOG(LogTemp, Warning, TEXT("Yaw: %f Pitch: %f MovementVector: %s"), Yaw, Pitch, *MovementVector.ToString());
	// get forward vector
	const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

	// get right vector 
	const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

	auto Movement = Cast<UPMOMovementComponent>(GetCharacterMovement());
	if (!Movement)
	{
		return;
	}

	Movement->ProcessMoveInputs(MovementVector, Yaw, Pitch);

	//AddMovementInput(ForwardDirection, MovementVector.Y);
	//AddMovementInput(RightDirection, MovementVector.X);
}

void APMOClientCharacter::StopMoving(const FInputActionValue& Value)
{
	bMoving = false;
	// This would most likely be cleared in PMOSystem anyways, but clear it mmanually here too
	auto Movement = Cast<UPMOMovementComponent>(GetCharacterMovement());
	if (Movement)
	{
		bool bPressed = false;
		Movement->CollectInput(input::XAxisMovement, bPressed);
		Movement->CollectInput(input::YAxisMovement, bPressed);
	}
}

void APMOClientCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();
	
	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void APMOClientCharacter::InputAction(input::InputType Input)
{
	UE_LOG(LogTemp, Warning, TEXT("InputAction (before): %d Pressed"), int(Input));
	auto Movement = Cast<UPMOMovementComponent>(GetCharacterMovement());
	if (Movement)
	{
		bool bPressed = true;
		UE_LOG(LogTemp, Warning, TEXT("InputAction: %d Pressed"), int(Input));
		Movement->CollectInput(Input, bPressed);
	}
}
