// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PMOClient : ModuleRules
{
	public PMOClient(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        CppStandard = CppStandardVersion.Cpp20;

        PublicDependencyModuleNames.AddRange(new string[] {"Core", "PMO", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "UMG", "Slate" });
		
		var Directory = ModuleDirectory + "/../../Plugins/PMO/Source/ThirdParty/pmo/build";
		
		PublicAdditionalLibraries.AddRange(new string[] {
			Directory+"/Release/pmo_library.lib",
			Directory+"/Release/flecs.lib",
			Directory+"/Release/Jolt.lib",
			Directory+"/../third_party/libsodium-1.0.18/Build/Release/x64/libsodium.lib",
		});

		PublicIncludePaths.AddRange(
			new string[] {
				Directory+"/../src/",
				Directory+"/_deps/flatbuffers-src/include/",
				Directory+"/_deps/jolt-src/",
                Directory+"/_deps/flecs-src/include/",
				Directory+"/../third_party/libsodium-1.0.18/src/libsodium/include/",
		});
	}
}
