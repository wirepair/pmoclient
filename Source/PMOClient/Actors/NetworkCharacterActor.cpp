// Fill out your copyright notice in the Description page of Project Settings.


#include "PMOClient/Actors/NetworkCharacterActor.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "ecs/character/components.h"


// Sets default values
ANetworkCharacterActor::ANetworkCharacterActor(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer.SetDefaultSubobjectClass<UPMONetworkMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	EntityComponent = CreateDefaultSubobject<UPMOEntityComponent>(TEXT("EntityComponent"));
	AttributesComponent = CreateDefaultSubobject<UPMOAttributesComponent>(TEXT("AttributesComponent"));
	CharacterStateComponent = CreateDefaultSubobject<UPMOCharacterStateComponent>(TEXT("CharacterStateComponent"));
	CombatComponent = CreateDefaultSubobject<UPMOCombatComponent>(TEXT("CombatComponent"));
	SetActorEnableCollision(false);
}

// Called when the game starts or when spawned
void ANetworkCharacterActor::BeginPlay()
{
	Super::BeginPlay();
}

void ANetworkCharacterActor::Update(FVector &Position, FVector &Velocity, FQuat &Rotation)
{
	Position.Z += CapsuleSize;
	SetActorLocationAndRotation(Position, Rotation);
	auto Movement = Cast<UPMONetworkMovementComponent>(GetCharacterMovement());
	if (!Movement)
	{
		return;
	}
	Movement->Velocity = Velocity;
	Movement->UpdateComponentVelocity();
}

void ANetworkCharacterActor::UpdateStats(units::Attributes& Attributes, units::TraitAttributes& Traits)
{
	AttributesComponent->Update(Attributes, Traits);
}

// Called every frame
void ANetworkCharacterActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

