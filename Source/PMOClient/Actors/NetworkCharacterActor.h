// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"
#include "PMOClient/PMOComponents/PMOCombatComponent.h"
#include "PMOClient/PMOComponents/PMOEntityComponent.h"
#include "PMOClient/PMOComponents/PMOAttributesComponent.h"
#include "PMOClient/PMOComponents/PMONetworkMovementComponent.h"
#include "PMOClient/PMOComponents/PMOCharacterStateComponent.h"
#include "NetworkCharacterActor.generated.h"



struct NetworkCharacterRef
{
	ANetworkCharacterActor* Actor;
};

UCLASS()
class PMOCLIENT_API ANetworkCharacterActor : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ANetworkCharacterActor(const FObjectInitializer& ObjectInitializer);

	/** Update our player position velocity and rotation */
	void Update(FVector &Position, FVector &Velocity, FQuat &Rotation);

	/** Update our player stats such as health, or our traits such as int/wis/dex */
	void UpdateStats(units::Attributes& Attributes, units::TraitAttributes& Traits);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PMO, meta = (AllowPrivateAccess = "true"))
	class UPMOEntityComponent* EntityComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PMO, meta = (AllowPrivateAccess = "true"))
	class UPMOCombatComponent* CombatComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PMO, meta = (AllowPrivateAccess = "true"))
	class UPMOAttributesComponent* AttributesComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PMO, meta = (AllowPrivateAccess = "true"))
	class UPMOCharacterStateComponent* CharacterStateComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PMO)
	float CapsuleSize = 90.f;

};
