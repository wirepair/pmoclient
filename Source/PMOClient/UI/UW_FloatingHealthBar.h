// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "IFloatingWidget.h"
#include "Components/ProgressBar.h"
#include "PMOClient/PMOComponents/PMOAttributesComponent.h"
#include "UWFloatingWidgetComponent.h"
#include "UW_FloatingHealthBar.generated.h"

/**
 * 
 */
UCLASS()
class PMOCLIENT_API UUW_FloatingHealthBar : public UUserWidget, public IIFloatingWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;

	virtual void SetComponent(class UUWFloatingWidgetComponent* Component) override;

	void OnUpdateAttributes();
	
	void SetMaxHealth(float MaxHealthValue);

	void SetMaxEnergy(float MaxEnergyValue);

	void SetMaxShield(float MaxShieldValue);

	void UpdateHealth(float NewValue);

	void UpdateShield(float NewValue);

	void UpdateEnergy(float NewValue);

protected:

	UPROPERTY(meta=(BindWidget))
	UProgressBar* HealthBar;
	float MaxHealthValue;
	float HealthValue;

	UPROPERTY(meta=(BindWidget))
	UProgressBar* ShieldBar;
	float MaxShieldValue;
	float ShieldValue;

private:
	TObjectPtr<UUWFloatingWidgetComponent> Owner;
	TObjectPtr<UPMOAttributesComponent> Attributes;
	FNumberFormattingOptions NumberFormatOptions;
};
