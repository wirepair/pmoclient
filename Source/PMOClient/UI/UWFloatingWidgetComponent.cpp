// Fill out your copyright notice in the Description page of Project Settings.


#include "PMOClient/UI/UWFloatingWidgetComponent.h"
#include "IFloatingWidget.h"
#include "Kismet/KismetMathLibrary.h"


void UUWFloatingWidgetComponent::BeginPlay()
{
    InitWidget();
    auto FloatingWidget = Cast<IIFloatingWidget>(GetWidget());
    if (FloatingWidget)
    {
        FloatingWidget->SetComponent(this);
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("FloatingWidget not set, GetWidget() returned null or not a FloatingWidget, can't call SetComponent"));
    }
    SetCollisionEnabled(ECollisionEnabled::NoCollision); // so we don't collide the camera manager with this component
    Super::BeginPlay();
}

void UUWFloatingWidgetComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    FacePlayer();
}

void UUWFloatingWidgetComponent::FacePlayer()
{
    auto FirstPlayer = GetWorld()->GetFirstPlayerController();
    if (!FirstPlayer || !FirstPlayer->PlayerCameraManager)
    {
        return;
    }

    auto WidgetLocation = GetComponentLocation();
    auto TargetLocation = FirstPlayer->PlayerCameraManager->GetTargetLocation();
    auto NewRotation = UKismetMathLibrary::FindLookAtRotation(WidgetLocation, TargetLocation);
    SetWorldRotation(NewRotation);
}
