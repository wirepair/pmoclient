// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UWMovableWidget.generated.h"

/**
 * 
 */
UCLASS()
class PMOCLIENT_API UUWMovableWidget : public UUserWidget
{
	GENERATED_BODY()
};
