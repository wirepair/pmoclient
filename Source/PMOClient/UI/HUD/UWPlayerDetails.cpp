// Fill out your copyright notice in the Description page of Project Settings.


#include "PMOClient/UI/HUD/UWPlayerDetails.h"
#include "PMOClient/PMOClientCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "ecs/units.h"

bool UUWPlayerDetails::Initialize()
{
    NumberFormatOptions.MinimumIntegralDigits = 1;
    NumberFormatOptions.MaximumIntegralDigits = 4;
    NumberFormatOptions.MinimumFractionalDigits = 0;
    NumberFormatOptions.MaximumFractionalDigits = 0;
    SetMaxHealth(units::MaxHealth);
    SetMaxShield(units::MaxShield);
    auto Char = Cast<APMOClientCharacter>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetCharacter());
    if (Char)
    {
        Attributes = Char->GetOwner()->GetComponentByClass<UPMOAttributesComponent>();
        Attributes->GetAttributesUpdatedDelegate().AddUObject(this, &ThisClass::OnUpdateAttributes);
        UE_LOG(LogTemp, Error, TEXT("UWPlayerDetails::SetComponent added delegate for OnUpdate"));
    }

    return Super::Initialize();
}


void UUWPlayerDetails::OnUpdateAttributes()
{
    UE_LOG(LogTemp, Log, TEXT("UWPlayerDetails::OnUpdateAttributes called"));
    UpdateHealth(Attributes->GetHealth());
    UpdateShield(Attributes->GetShield());
}

void UUWPlayerDetails::SetMaxHealth(float NewMaxHealthValue)
{
    MaxHealthValue = NewMaxHealthValue;
    UE_LOG(LogTemp, Error, TEXT("UWPlayerDetails::SetMaxHealth %f"), MaxHealthValue);
}

void UUWPlayerDetails::SetMaxShield(float NewMaxShieldValue)
{
    MaxShieldValue = NewMaxShieldValue;
}

void UUWPlayerDetails::UpdateHealth(float NewValue)
{
    HealthValue = NewValue;
    UE_LOG(LogTemp, Warning, TEXT("%f/%f = %f"), HealthValue, MaxHealthValue, HealthValue / MaxHealthValue);
    if (HealthBar)
    {
        HealthBar->SetPercent(HealthValue / MaxHealthValue);
    }
    else
    {
        UE_LOG(LogTemp, Log, TEXT("UWPlayerDetails::UpdateHealth progress bar is null"));
    }
}

void UUWPlayerDetails::UpdateShield(float NewValue)
{
    if (ShieldBar)
    {
        ShieldBar->SetPercent(NewValue / MaxShieldValue);
    }
}