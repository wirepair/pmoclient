// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ProgressBar.h"
#include "PMOClient/PMOComponents/PMOAttributesComponent.h"
#include "PMOClient/UI/HUD/UWMovableWidget.h"
#include "UWPlayerDetails.generated.h"

/**
 * 
 */
UCLASS()
class PMOCLIENT_API UUWPlayerDetails : public UUWMovableWidget
{
	GENERATED_BODY()
	
	virtual bool Initialize() override;

	void OnUpdateAttributes();

	void SetMaxHealth(float MaxHealthValue);

	void SetMaxEnergy(float MaxEnergyValue);

	void SetMaxShield(float MaxShieldValue);

	void UpdateHealth(float NewValue);

	void UpdateShield(float NewValue);

	void UpdateEnergy(float NewValue);

protected:

	UPROPERTY(meta = (BindWidget))
	UProgressBar* HealthBar;
	float MaxHealthValue;
	float HealthValue;

	UPROPERTY(meta = (BindWidget))
	UProgressBar* ShieldBar;
	float MaxShieldValue;
	float ShieldValue;

private:
	TObjectPtr<UPMOAttributesComponent> Attributes;
	FNumberFormattingOptions NumberFormatOptions;
};
