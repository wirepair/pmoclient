// Fill out your copyright notice in the Description page of Project Settings.


#include "UW_FloatingHealthBar.h"
#include "ecs/units.h"

bool UUW_FloatingHealthBar::Initialize()
{
    NumberFormatOptions.MinimumIntegralDigits = 1;
    NumberFormatOptions.MaximumIntegralDigits = 4;
    NumberFormatOptions.MinimumFractionalDigits = 0;
    NumberFormatOptions.MaximumFractionalDigits = 0;
 
    return Super::Initialize();
}

void UUW_FloatingHealthBar::SetComponent(class UUWFloatingWidgetComponent* Component)
{
    Owner = Component;
    if (Owner)
    {
        Attributes = Owner->GetOwner()->GetComponentByClass<UPMOAttributesComponent>();
        Attributes->GetAttributesUpdatedDelegate().AddUObject(this, &ThisClass::OnUpdateAttributes);
        UE_LOG(LogTemp, Error, TEXT("UUW_FloatingHealthBar::SetComponent added delegate for OnUpdate"));
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("UUW_FloatingHealthBar::SetComponent failed to init delegate"));
    }
    SetMaxHealth(units::MaxHealth);
    SetMaxShield(units::MaxShield);
}

void UUW_FloatingHealthBar::OnUpdateAttributes()
{
    UE_LOG(LogTemp,Log, TEXT("UUW_FloatingHealthBar::OnUpdateAttributes called"));
    UpdateHealth(Attributes->GetHealth());
    UpdateShield(Attributes->GetShield());
}

void UUW_FloatingHealthBar::SetMaxHealth(float NewMaxHealthValue)
{
    MaxHealthValue = NewMaxHealthValue;
    UE_LOG(LogTemp, Error, TEXT("UUW_FloatingHealthBar::SetMaxHealth %f"), MaxHealthValue);
}

void UUW_FloatingHealthBar::SetMaxShield(float NewMaxShieldValue)
{
    MaxShieldValue = NewMaxShieldValue;
}

void UUW_FloatingHealthBar::UpdateHealth(float NewValue)
{
    HealthValue = NewValue;
    UE_LOG(LogTemp, Warning, TEXT("%f/%f = %f"), HealthValue, MaxHealthValue, HealthValue / MaxHealthValue);
    if (HealthBar)
    {
        HealthBar->SetPercent(HealthValue / MaxHealthValue);
    }
    else
    {
        UE_LOG(LogTemp, Log, TEXT("UUW_FloatingHealthBar::UpdateHealth progress bar is null"));
    }
}

void UUW_FloatingHealthBar::UpdateShield(float NewValue)
{
    if (ShieldBar)
    {
        ShieldBar->SetPercent(NewValue / MaxShieldValue);
    }
}