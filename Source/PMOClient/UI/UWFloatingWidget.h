// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UWFloatingWidget.generated.h"

/**
 * 
 */
UCLASS()
class PMOCLIENT_API UUWFloatingWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void SetComponent(class UUWFloatingWidgetComponent* Component) 
	{
		Owner = Component; 
	};
	
protected:
	class UUWFloatingWidgetComponent* Owner;
};
